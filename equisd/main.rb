require 'sinatra'
require 'sinatra/reloader'

get '/' do
  erb :index
end

get '/acerca' do
  #erb :page, :layout => :layout_simple
  erb :page
end

get '/servicios/detalles/:servicio_id' do
  @param_passed = params[:servicio_id]
  erb :page
end

get '/servicios/?:servicio_id?' do
  @param_passed = params[:servicio_id]
  erb :page
end

get /\A\/productos\/([0-9]+)\z/ do
  @param_passed = params['captures'].first
  erb :page
end